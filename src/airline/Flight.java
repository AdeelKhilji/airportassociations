/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package airline;
import java.util.Date;//Importing java utils for Date
/**
 * Class Flight
 * @author Adeel Khilji
 */
public class Flight 
{
    private int number;//Declaring instance variable number type int
    private Date date;//Declaring instance variable date type Date
    
    /**
     * Constructor with two parameters
     * @param number int
     * @param date Date
     */
    public Flight(int number, Date date)
    {
        this.number = number;//Assigning number to this.number
        this.date = date;//Assigning date to this.date
    }
    
    /**
     * getNumber() - getter method of type int
     * @return int
     */
    public int getNumber()
    {
        return this.number;
    }
    /**
     * getDate() - getter method of type Date
     * @return Date
     */
    public Date getDate()
    {
        return this.date;
    }
}
