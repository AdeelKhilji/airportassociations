/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package airline;

/**
 * Class Airport
 * @author Adeel Khilji
 */
public class Airport 
{
    private String code, city;//Declaring instance variables code and city of type String
    
    /**
     * Constructor with two parameters
     * @param code String
     * @param city String
     */
    public Airport(String code, String city)
    {
        this.code = code;//Assigning code to this.code
        this.city = city;//Assigning city to this.city
    }
    /**
     * getCode() - getter method of type String
     * @return String
     */
    public String getCode()
    {
        return this.code;
    }
    /**
     * getCity() - getter method of type String
     * @return String
     */
    public String getCity()
    {
        return this.city;
    }
}
