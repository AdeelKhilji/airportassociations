/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package airline;

/**
 * Class Airplane
 * @author Adeel Khilji
 */
public class Airplane 
{
    int Id, numberOfSeats;//Declaring instance variables Id, numberOfSeats of type int
    String make, model;//Declaring instance variables make, model of type String
    
    /**
     * Constructor with four parameters
     * @param Id int
     * @param numberOfSeats int
     * @param make String
     * @param model String
     */
    public Airplane(int Id, int numberOfSeats, String make, String model)
    {
        this.Id = Id;//Assigning Id to this.Id
        this.numberOfSeats = numberOfSeats;//Assigning numberOfSeats to this.numberOfSeats
        this.make = make;//Assigning make to this.make
        this.model = model;//Assigning model to this.model
    }
    /**
     * getId() - getter method that returns int
     * @return int
     */
    public int getId()
    {
        return this.Id;
    }
    /**
     * getNumberOfSeats() - getter method that returns int
     * @return int
     */
    public int getNumberOfSeats()
    {
        return this.numberOfSeats;
    }
    /**
     * getMake() - getter method that returns String
     * @return String
     */
    public String getMake()
    {
        return this.make;
    }
    /**
     * getModel() - getter method that returns String
     * @return String
     */
    public String getModel()
    {
        return this.model;
    }
}
