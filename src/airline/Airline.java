/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package airline;
/**
 * Class Airline
 * @author Adeel Khilji
 */
public class Airline 
{
    private String name, shortCode;//Declaring instance variables name, shortCode of type String
    
    /**
     * Constructor with two parameters
     * @param name String
     * @param shortCode String
     */
    public Airline(String name, String shortCode)
    {
        this.name = name;//Assigning name to this.name
        this.shortCode = shortCode;//Assigning shortCode to this.shortCode
    }
    /**
     * getName() - getter method that returns String
     * @return String
     */
    public String getName()
    {
        return this.name;
    }
    /**
     * getShortCode() - getter method that returns String
     * @return String
     */
    public String getShortCode()
    {
        return this.shortCode;
    }
}
