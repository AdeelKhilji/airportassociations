/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package airline;
import java.util.Date;
/**
 * Class Simulation
 * @author Adeel Khilji
 */
public class Simulation 
{
    /**
     * Main method
     * @param args String 
     */
    public static void main(String[] args)
    {
        Date date = new Date();//Date object
        Airport airport = new Airport("YYZ","Toronto");//Airport object
        Airline airline = new Airline("WestJet Airlines","WS");//Airline object
        Airplane airplane = new Airplane(737,174,"WestJet", "CFM56-7B");//Airplane object
        Flight flight = new Flight(8387,date);//Flight object
        
        System.out.println("AIRPORT INFO:\t\tCITY: ");//Displaying flight information
        System.out.println(airport.getCode() + "\t\t\t" + airport.getCity()+ "\n");//Displaying airport code and city
        System.out.print("AIRLINE: \t\t" + airline.getShortCode() + "/" + airline.getName());//Displaying shortCode and name
        //Displaying Airplane Id make model and number of seats
        System.out.println("\tAIRPLANE: " + airplane.getId() + "\t" + airplane.getMake() + "/" + airplane.getModel() + "\nNUMBER OF SEATS: \t" + airplane.getNumberOfSeats());
        System.out.println("\nFLIGHT INFO: " + flight.getNumber() + "\t\t\t\tDATE: " + flight.getDate());//Displaying flight number and date
    }
}
